//
//  ViewController.h
//  TechCurrentPosition
//
//  Created by 田中 賢治 on 2014/01/17.
//  Copyright (c) 2014年 com.ktanaka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController<CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
}

@end
